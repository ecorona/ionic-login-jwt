// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  mapsApiKey: 'AIzaSyAIl2_eZ4JKdDaFoI_0kcR5MhCP3DLYk8k',
  apiUrl: '/api/v1',
  socketServer: '/',
  oneSignalAppId: '98fca6d6-9e70-4abd-8a05-e3717ce544fe',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/dist/zone-error'; // Included with Angular CLI.
