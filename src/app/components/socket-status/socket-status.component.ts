import { AuthService } from './../../auth/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { SocketsService } from 'src/app/services/sockets.service';

@Component({
  selector: 'app-socket-status',
  templateUrl: './socket-status.component.html',
  styleUrls: ['./socket-status.component.scss'],
})
export class SocketStatusComponent implements OnInit, OnDestroy {
  socketConectado: boolean;
  unsubscribeAll: Subject<boolean>;
  constructor(
    private readonly socketsService: SocketsService,
    private readonly auth: AuthService
  ) {
    this.unsubscribeAll = new Subject();
  }

  ngOnDestroy(): void {
    this.unsubscribeAll.next(true);
    this.unsubscribeAll.complete();
  }

  ngOnInit() {
    /**
     * Conectarnos al socket para ver si esta conectado / desconectado
     */
    this.socketsService.conectado$
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe({
        next: (socketConectado) => {
          this.socketConectado = socketConectado;
          if (socketConectado) {
            this.auth.getUser().subscribe();
          }
        },
      });
  }
}
