import { LoadingService } from './../../services/loading.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-loading-status',
  templateUrl: './loading-status.component.html',
  styleUrls: ['./loading-status.component.scss'],
})
export class LoadingStatusComponent implements OnInit, OnDestroy {
  cargando: boolean;
  unsubscribeAll: Subject<boolean>;
  constructor(private readonly loading: LoadingService) {
    this.unsubscribeAll = new Subject();
  }

  ngOnDestroy(): void {
    this.unsubscribeAll.next(true);
    this.unsubscribeAll.complete();
  }

  ngOnInit() {
    this.loading.loading$.pipe(takeUntil(this.unsubscribeAll)).subscribe({
      next: (cargando) => {
        this.cargando = cargando;
      },
    });
  }
}
