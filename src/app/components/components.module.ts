import { LoadingStatusComponent } from './loading-status/loading-status.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { SocketStatusComponent } from './socket-status/socket-status.component';

@NgModule({
  declarations: [SocketStatusComponent, LoadingStatusComponent],
  imports: [IonicModule, CommonModule],
  exports: [SocketStatusComponent, LoadingStatusComponent],
})
export class ComponentsModule {}
