import { VerificacionResponse } from './verificacion-response.dto';
import { VerificacionRequestData } from './verificacion-request-data.dto';
import { VerificacionService } from './verificacion.service';
import { ViewWillEnter } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Preferences } from '@capacitor/preferences';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { emailValidator } from '../validators/email.validators';
import { Router } from '@angular/router';
import { ErrorResponse } from '../registro/registro-error-response.dto';

@Component({
  selector: 'app-verificacion',
  templateUrl: './verificacion.page.html',
  styleUrls: ['./verificacion.page.scss'],
})
export class VerificacionPage implements OnInit, ViewWillEnter {
  formVerificacion: FormGroup;
  isSubmitted = false;
  error: string;
  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly verificacionService: VerificacionService,
    private readonly router: Router
  ) {}
  get errorControl() {
    return this.formVerificacion.controls;
  }
  //cargamos el email que ya haya solicitado registro en este dispositivo.
  async ionViewWillEnter(): Promise<void> {
    const { value } = await Preferences.get({ key: 'emailRegistro' });
    this.formVerificacion.get('email').setValue(value);
  }

  ngOnInit() {
    this.formVerificacion = this.formBuilder.group({
      email: ['', [Validators.required, emailValidator]],
      code: ['', [Validators.required, Validators.minLength(5)]],
    });
  }

  async submitForm() {
    this.isSubmitted = true;
    if (!this.formVerificacion.valid) {
      console.log('El formulario de registro no es correcto.');
      return false;
    } else {
      const formData: VerificacionRequestData =
        this.formVerificacion.getRawValue();
      this.error = '';
      console.log('form:', formData);
      this.verificacionService.verificar(formData).subscribe({
        next: async (response: VerificacionResponse) => {
          console.log('verificacionService.verificar->response: ', response);
          //pasarlo a la página de login y borrar la anterior.
          this.router.navigate(['/login'], {
            replaceUrl: true,
            skipLocationChange: true,
          });
        },
        error: (errorResponse: ErrorResponse) => {
          this.error =
            errorResponse.error?.message ||
            errorResponse.message ||
            'No se pudo verificar el registro, inténtelo más tarde.';
          console.log(
            'verificacionService.verificar->error-response: ',
            errorResponse
          );
        },
      });
    }
  }
}
