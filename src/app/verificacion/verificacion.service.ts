import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { VerificacionResponse } from './verificacion-response.dto';
import { VerificacionRequestData } from './verificacion-request-data.dto';

@Injectable({
  providedIn: 'root',
})
export class VerificacionService {
  constructor(private readonly http: HttpClient) {}

  verificar(data: VerificacionRequestData): Observable<VerificacionResponse> {
    return this.http.put<VerificacionResponse>(
      environment.apiUrl + '/auth/verify/email',
      data
    );
  }
}
