import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  OnDestroy,
  NgZone,
} from '@angular/core';
import { GoogleMap } from '@capacitor/google-maps';
import {
  CameraIdleCallbackData,
  CameraMoveStartedCallbackData,
  MapClickCallbackData,
  MarkerClickCallbackData,
} from '@capacitor/google-maps/dist/typings/definitions';
import { ViewDidEnter } from '@ionic/angular';
import { Subject, takeUntil } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LoadingService } from '../services/loading.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit, OnDestroy, ViewDidEnter {
  @ViewChild('map')
  mapRef: ElementRef<HTMLElement>;
  map: GoogleMap;
  markerId: string;
  cargando: boolean;
  unsubscribeAll: Subject<boolean>;
  constructor(private readonly loadingService: LoadingService) {
    this.unsubscribeAll = new Subject<boolean>();
  }
  ionViewDidEnter(): void {
    this.createMap();
  }
  ngOnDestroy(): void {
    this.map?.destroy();
    this.unsubscribeAll.next(true);
    this.unsubscribeAll.complete();
  }

  ngOnInit() {
    this.loadingService.loading$
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe({
        next: (cargando) => {
          this.cargando = cargando;
        },
      });
  }

  async createMap() {
    this.loadingService.start();
    this.map = await GoogleMap.create({
      id: 'my-cool-map',
      element: this.mapRef.nativeElement,
      apiKey: environment.mapsApiKey,
      config: {
        center: {
          lat: 17.0732,
          lng: -96.7266,
        },
        zoom: 12,
      },
    });
    this.loadingService.stop();

    this.map.setOnMapClickListener(async (data?: MapClickCallbackData) => {
      console.log('MapClickCallbackData', data);
      if (this.markerId) {
        await this.map.removeMarker(this.markerId);
      }
      this.markerId = await this.map.addMarker({
        coordinate: {
          lat: data.latitude,
          lng: data.longitude,
        },
        title: 'Clic',
        draggable: true,
        snippet: 'Hola Mundo',
      });
      this.map.setCamera({
        animate: true,
        animationDuration: 500,
        coordinate: {
          lat: data.latitude,
          lng: data.longitude,
        },
        zoom: 16,
      });
    });

    this.map.setOnMarkerClickListener(
      async (data?: MarkerClickCallbackData) => {
        console.log('MarkerClickCallbackData', data);
      }
    );

    this.map.setOnCameraMoveStartedListener(
      (data?: CameraMoveStartedCallbackData) => {
        console.log('CameraMoveStartedCallbackData', data);
      }
    );

    this.map.setOnCameraIdleListener((data?: CameraIdleCallbackData) => {
      console.log('CameraIdleCallbackData', data);
    });

    await this.map.enableTrafficLayer(true);
    await this.map.enableCurrentLocation(true);
    await this.map.enableAccessibilityElements(true);
  }
}
