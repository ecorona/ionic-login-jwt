export const APP_PAGES = [
  { title: 'Home', url: '/home', icon: 'home', requireSession: true },
  { title: 'Perfil', url: '/perfil', icon: 'person', requireSession: true },
  {
    title: 'Acerca de...',
    url: '/acerca',
    icon: 'cog',
    requireSession: false,
  },
  {
    title: 'Registrarme',
    url: '/registro',
    icon: 'person-add',
    requireSession: false,
    hideOnSession: true,
  },
  {
    title: 'Verificación',
    url: '/verificacion',
    icon: 'checkbox',
    requireSession: false,
    hideOnSession: true,
  },
  {
    title: 'Configuración',
    url: '/configuracion',
    icon: 'information-circle',
    requireSession: false,
  },
  {
    title: 'Notificaciones',
    url: '/notificaciones',
    icon: 'alert-circle',
    requireSession: true,
  },
  {
    title: 'Mapa',
    url: '/map',
    icon: 'map',
    requireSession: false,
  },
  {
    title: 'Qr Scan',
    url: '/qr-scan',
    icon: 'qr-code',
    requireSession: false,
  },
  {
    title: 'Cámara',
    url: '/camera',
    icon: 'camera',
    requireSession: false,
  },
];
