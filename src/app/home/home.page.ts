import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { UserModel } from '../auth/model/user.model';
import { Geolocation, Position } from '@capacitor/geolocation';
import { Haptics, ImpactStyle } from '@capacitor/haptics';
import { Device, DeviceInfo } from '@capacitor/device';
import { LocalNotifications } from '@capacitor/local-notifications';
import { ConnectionStatus, Network } from '@capacitor/network';
import { Share } from '@capacitor/share';
import { Toast } from '@capacitor/toast';
import { LoadingService } from '../services/loading.service';
import { runInThisContext } from 'vm';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {
  user: UserModel;
  posicion: Position;
  networkStatus: ConnectionStatus;
  deviceInfo: DeviceInfo;
  unsubscribeAll: Subject<boolean>;
  cargando: boolean;
  constructor(
    private readonly auth: AuthService,
    private readonly zone: NgZone,
    private readonly loadingService: LoadingService
  ) {
    this.unsubscribeAll = new Subject<boolean>();
  }
  ngOnDestroy(): void {
    this.unsubscribeAll.next(true);
    this.unsubscribeAll.complete();
  }

  async ngOnInit() {
    this.auth.user$.pipe(takeUntil(this.unsubscribeAll)).subscribe({
      next: (user) => {
        this.user = user;
      },
    });
    this.loadingService.loading$
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe({
        next: (cargando) => {
          this.cargando = cargando;
        },
      });
    this.deviceInfo = await Device.getInfo();
    this.networkStatus = await Network.getStatus();
  }

  async obtenerPosicion() {
    this.loadingService.start();
    await Geolocation.requestPermissions()
      .then(async (result) => {
        if (
          result.coarseLocation === 'granted' ||
          result.location === 'granted'
        ) {
          await Haptics.impact({ style: ImpactStyle.Light });
          const posicion = await Geolocation.getCurrentPosition({
            maximumAge: 5000,
            timeout: 5000,
            enableHighAccuracy: true,
          });
          this.zone.run(async () => {
            this.loadingService.stop();
            this.posicion = posicion;
          });
          await Haptics.impact({ style: ImpactStyle.Medium });
        }
      })
      .catch(async (error) => {
        this.loadingService.stop();
        if (error.message) {
          await Toast.show({
            text: error.message,
          });
        }
      });
  }

  async enviarNotificacion() {
    await LocalNotifications.requestPermissions().then(async (result) => {
      if (result.display === 'granted') {
        await LocalNotifications.schedule({
          notifications: [
            {
              title: 'Notificacion 1',
              body: 'Prueba de notificacion',
              id: 1,
              largeBody: 'Este es el texto mas grande.',
            },
          ],
        });
      } else if (result.display === 'denied') {
        await Toast.show({
          text: 'No se ha dado permiso para las notificaciones locales.',
        });
      }
    });
  }

  async compartir() {
    try {
      this.loadingService.start();
      const puedeCompartir = await Share.canShare();
      if (puedeCompartir) {
        await Share.share({
          title: 'Esto esta muy bien',
          text: 'Te comparto este texto',
          url: 'https://xst.mx/',
          dialogTitle: 'Compartir con mis amigos',
        });
      } else {
        await Toast.show({
          text: 'No se puede compartir.',
        });
      }
      this.loadingService.stop();
    } catch (error) {
      this.loadingService.stop();
      if (error.message) {
        await Toast.show({
          text: error.message,
        });
      }
    }
  }

  async toastNativo() {
    await Toast.show({
      text: 'Hello!',
    });
  }
}
