import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { UserModel } from '../auth/model/user.model';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit, OnDestroy {
  user: UserModel = null;
  unsubscribeAll: Subject<boolean>;
  constructor(private readonly auth: AuthService) {
    this.unsubscribeAll = new Subject<boolean>();
  }
  ngOnDestroy(): void {
    this.unsubscribeAll.next(true);
    this.unsubscribeAll.complete();
  }

  ngOnInit() {
    this.auth.user$.pipe(takeUntil(this.unsubscribeAll)).subscribe({
      next: (user) => {
        this.user = user;
      },
    });
  }

  salir() {
    this.auth.logout();
  }
}
