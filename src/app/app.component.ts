import { PushService } from './services/push.service';
import { environment } from './../environments/environment';
import { UserModel } from './auth/model/user.model';
import { AuthService } from './auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { App, RestoredListenerEvent } from '@capacitor/app';
import OneSignal from 'onesignal-cordova-plugin';
import { OpenedEvent } from 'onesignal-cordova-plugin/dist/models/NotificationOpened';
import { Platform } from '@ionic/angular';
import NotificationReceivedEvent from 'onesignal-cordova-plugin/dist/NotificationReceivedEvent';
import { APP_PAGES } from './app-pages.array';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  user: UserModel = null;
  public appPages = APP_PAGES;

  constructor(
    public readonly auth: AuthService,
    private readonly push: PushService,
    private readonly platform: Platform
  ) {}
  ngOnInit(): void {
    if (this.platform.is('mobile')) {
      OneSignal.setAppId(environment.oneSignalAppId);
      OneSignal.setNotificationOpenedHandler((jsonData: OpenedEvent) => {
        this.push.procesarNotificacionAbierta(jsonData);
      });
      OneSignal.setNotificationWillShowInForegroundHandler(
        (event: NotificationReceivedEvent) => {
          this.push.procesarNotificacionFrontal(event);
        }
      );
      OneSignal.promptForPushNotificationsWithUserResponse((accepted) => {
        this.push.aceptarNotificaciones(accepted);
      });
    }

    /**
     * Conectarnos al usuario
     */
    this.auth.user$.pipe().subscribe({
      next: (user) => {
        this.user = user;
      },
    });

    App.addListener('appRestoredResult', (data: RestoredListenerEvent) => {
      console.log('Aplicación restaurada de estado anterior, data:', data);
    });

    App.addListener('appStateChange', ({ isActive }) => {
      console.log('El estado de la app ha cambiado, acivo: ', isActive);
    });
  }

  salir() {
    this.auth.logout();
  }
}
