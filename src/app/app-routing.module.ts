import { SessionGuard } from './auth/session.guard';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { UnSessionGuard } from './auth/un-session.guard';

const routes: Routes = [
  {
    path: 'home',
    canActivate: [SessionGuard],
    loadChildren: () =>
      import('./home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: 'login',
    canActivate: [UnSessionGuard],
    loadChildren: () =>
      import('./login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'perfil',
    canActivate: [SessionGuard],
    loadChildren: () =>
      import('./perfil/perfil.module').then((m) => m.PerfilPageModule),
  },
  {
    path: 'acerca',
    loadChildren: () =>
      import('./acerca/acerca.module').then((m) => m.AcercaPageModule),
  },
  {
    path: 'configuracion',
    loadChildren: () =>
      import('./configuracion/configuracion.module').then(
        (m) => m.ConfiguracionPageModule
      ),
  },
  {
    path: 'notificaciones',
    loadChildren: () =>
      import('./notificaciones/notificaciones.module').then(
        (m) => m.NotificacionesPageModule
      ),
  },
  {
    path: 'map',
    loadChildren: () => import('./map/map.module').then((m) => m.MapPageModule),
  },
  {
    path: 'qr-scan',
    loadChildren: () =>
      import('./qr-scan/qr-scan.module').then((m) => m.QrScanPageModule),
  },
  {
    path: 'camera',
    loadChildren: () =>
      import('./camera/camera.module').then((m) => m.CameraPageModule),
  },
  {
    path: 'registro',
    loadChildren: () =>
      import('./registro/registro.module').then((m) => m.RegistroPageModule),
  },
  {
    path: 'verificacion',
    loadChildren: () =>
      import('./verificacion/verificacion.module').then(
        (m) => m.VerificacionPageModule
      ),
  },
];

routes.push({
  path: '**',
  redirectTo: '/home',
});
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
