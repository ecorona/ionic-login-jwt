/* eslint-disable @typescript-eslint/dot-notation */
import { Injectable } from '@angular/core';
import { OpenedEvent } from 'onesignal-cordova-plugin/dist/models/NotificationOpened';
import { Platform } from '@ionic/angular';
import { OSNotification } from '@awesome-cordova-plugins/onesignal';
import NotificationReceivedEvent from 'onesignal-cordova-plugin/dist/NotificationReceivedEvent';

@Injectable({
  providedIn: 'root',
})
export class PushService {
  constructor(public platform: Platform) {}

  procesarNotificacionAbierta(openedEvent: OpenedEvent) {
    console.log('procesarNotificacionAbierta:', openedEvent);
  }

  procesarNotificacionFrontal(event: NotificationReceivedEvent) {
    console.log('procesarNotificacionFrontal', event.getNotification());
  }

  aceptarNotificaciones(accepted: boolean) {
    console.log('Notificaciones aceptadas: ', accepted);
  }

  setTags(tags: { [key: string]: string }) {
    this.platform.ready().then(() => {
      if (this.platform.is('mobile') && window['plugins']?.OneSignal) {
        console.log('Estableciendo onesignal Tags:', tags);
        window['plugins'].OneSignal.sendTags(tags);
      }
    });
  }

  setExternalUserId(userId: string) {
    this.platform.ready().then(() => {
      if (this.platform.is('mobile') && window['plugins']?.OneSignal) {
        console.log('Estableciendo external_user_id:', userId);
        window['plugins'].OneSignal.setExternalUserId(userId, (results) => {
          // The results will contain push and email success statuses
          console.log('Results of setting external user id');
          console.log(results);

          // Push can be expected in almost every situation with a success status, but
          // as a pre-caution its good to verify it exists
          if (results.push && results.push.success) {
            console.log('Results of setting external user id push status:');
            console.log(results.push.success);
          }
        });
      }
    });
  }

  unSetExternalUserId() {
    if (this.platform.is('mobile') && window['plugins']?.OneSignal) {
      console.log('Removiendo external_user_id...');
      window['plugins'].OneSignal.removeExternalUserId((results) => {
        console.log('Results of removing external user id');
        console.log(results);
        // Push can be expected in almost every situation with a success status, but
        // as a pre-caution its good to verify it exists
        if (results.push && results.push.success) {
          console.log('Results of removing external user id push status:');
          console.log(results.push.success);
        }
      });
    }
  }
}
