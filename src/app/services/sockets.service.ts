import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { io, Socket } from 'socket.io-client';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SocketsService {
  private events = new Subject<{ event: string; data?: any }>();
  private conectado = new BehaviorSubject<boolean>(false);
  private socket = io(environment.socketServer, {
    transports: ['websocket'],
    autoConnect: false,
    reconnection: true,
    withCredentials: true,
    auth: {
      token: '',
    },
  });

  constructor() {}

  get events$(): Observable<{ event: string; data?: any }> {
    return this.events.asObservable();
  }

  get conectado$(): Observable<boolean> {
    return this.conectado.asObservable();
  }

  set token(token: string | null) {
    this.socket.auth = { token };
    if (token) {
      this.prepararEventos();
      this.socket.connect();
    } else if (!this.socket.disconnected) {
      this.socket.disconnect();
    }
  }

  private prepararEventos() {
    this.socket.on('connect', () => {
      this.conectado.next(true);
      this.events.next({ event: 'connect' });
      console.log('SocketService->Conectado');
    });

    this.socket.on('disconnect', (disconnectReason) => {
      this.conectado.next(false);
      this.events.next({ event: 'disconnect', data: disconnectReason });
      console.log('SocketService->Desconectado: ', disconnectReason);
    });
  }
}
