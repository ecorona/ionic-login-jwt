import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class ToastService {
  constructor(private toastController: ToastController) {}

  success(mensaje: string, duration = 3000): void {
    this.toastController
      .create({
        message: mensaje,
        duration,
      })
      .then((toast) => {
        toast.present();
      });
  }
  error(mensaje: string): void {
    this.toastController
      .create({
        message: mensaje,
        duration: 5000,
        color: 'danger',
      })
      .then((toast) => {
        toast.present();
      });
  }
}
