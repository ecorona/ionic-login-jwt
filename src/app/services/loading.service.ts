import { Observable, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LoadingService {
  privateNivel = 0;

  private loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );

  constructor() {}
  get loading$(): Observable<boolean> {
    return this.loading.asObservable();
  }
  get nivel(): number {
    return this.privateNivel;
  }
  set nivel(nivel: number) {
    if (nivel < 0) {
      this.privateNivel = 0;
    } else {
      this.privateNivel = nivel;
    }
    this.loading.next(this.privateNivel > 0);
  }

  start() {
    this.nivel++;
  }

  stop() {
    if (this.nivel) {
      setTimeout(() => {
        this.nivel--;
      }, 300);
    }
  }
}
