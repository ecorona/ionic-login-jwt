import { ToastService } from './../services/toast.service';
import { LoginResponse } from './../auth/model/login-response.model';
import {
  ChangeDetectorRef,
  Component,
  OnInit,
  NgZone,
  OnDestroy,
} from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { Platform, ViewWillEnter } from '@ionic/angular';
import { AuthService } from '../auth/auth.service';
import { emailValidator } from '../validators/email.validators';
import { Router } from '@angular/router';
import { Preferences } from '@capacitor/preferences';
import { App, AppInfo } from '@capacitor/app';
import { LoadingService } from '../services/loading.service';
import { Subject, takeUntil } from 'rxjs';

import {
  FacebookLogin,
  FacebookLoginResponse,
} from '@capacitor-community/facebook-login';
const FACEBOOK_PERMISSIONS = [
  'email',
  'user_birthday',
  'user_photos',
  'user_gender',
];

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit, ViewWillEnter, OnDestroy {
  loginForm: UntypedFormGroup;
  appInfo: AppInfo;
  cargando: boolean;
  unsubscribeAll: Subject<boolean>;
  facebookProfile: {
    id: string;
    name: string;
    email: string;
  };
  constructor(
    private readonly formBuilder: UntypedFormBuilder,
    private readonly authService: AuthService,
    private readonly toast: ToastService,
    private readonly zone: NgZone,
    private readonly router: Router,
    public readonly platform: Platform,
    public readonly loadingService: LoadingService
  ) {
    this.unsubscribeAll = new Subject<boolean>();
  }
  ngOnDestroy(): void {
    this.unsubscribeAll.next(true);
    this.unsubscribeAll.complete();
  }

  async ionViewWillEnter(): Promise<void> {
    const { value } = await Preferences.get({ key: 'emailRegistro' });
    this.loginForm.get('email').setValue(value);
  }

  async ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, emailValidator]],
      password: ['', [Validators.required]],
    });
    this.loadingService.loading$
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe({
        next: (cargando) => {
          this.cargando = cargando;
        },
      });
    this.platform.ready().then(async () => {
      if (this.platform.is('mobile')) {
        this.zone.run(async () => {
          this.appInfo = await App.getInfo();
        });
      }
    });
  }

  signIn() {
    if (this.loginForm.valid) {
      this.authService.login(this.loginForm.value).subscribe({
        next: async (loginResponse: LoginResponse) => {
          console.log('loginResponse', loginResponse);
          await Preferences.remove({
            key: 'emailRegistro',
          });
          this.router.navigate(['/home'], {
            relativeTo: null,
            replaceUrl: true,
            skipLocationChange: true,
          });
          this.toast.success(`Bienvenido`);
        },
        error: (error) => {
          console.log('Error al iniciar sesión:', error);
          this.toast.error('No se pudo iniciar sesión.');
        },
      });
    }
  }

  async facebookLogin() {
    const result = await FacebookLogin.login({
      permissions: FACEBOOK_PERMISSIONS,
    });
    console.log('Facebook login: ', result);
    if (result.accessToken) {
      // Login successful.
      console.log(`Facebook access token ${result.accessToken.token}`);
      const profile = await FacebookLogin.getProfile<{
        email: string;
        id: string;
        name: string;
      }>({
        fields: ['email', 'name'],
      });
      console.log(`Facebook profile: `, profile);
      console.log(`Facebook email ${profile.email}`);
      this.zone.run(() => {
        this.facebookProfile = profile;
      });
    }
  }
}
