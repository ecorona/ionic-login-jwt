import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import {
  BarcodeScanner,
  CameraDirection,
  SupportedFormat,
} from '@capacitor-community/barcode-scanner';
import { Haptics, ImpactStyle } from '@capacitor/haptics';
import { Toast } from '@capacitor/toast';

@Component({
  selector: 'app-qr-scan',
  templateUrl: './qr-scan.page.html',
  styleUrls: ['./qr-scan.page.scss'],
})
export class QrScanPage implements OnInit, OnDestroy {
  codigo: any;
  escaneando = false;
  constructor(private readonly ngZone: NgZone) {}
  ngOnDestroy(): void {
    this.cancelar();
  }

  async ngOnInit() {
    try {
      await BarcodeScanner.prepare();
    } catch (error) {
      if (error.message) {
        await Toast.show({
          text: error.message,
        });
      }
    }
  }

  async scan() {
    try {
      // Check camera permission
      const resultPermiso = await BarcodeScanner.checkPermission({
        force: true,
      });

      if (resultPermiso.granted) {
        // make background of WebView transparent
        // note: if you are using ionic this might not be enough, check below

        BarcodeScanner.hideBackground();

        this.ngZone.run(async () => {
          this.escaneando = true;
        });
        await Haptics.impact({ style: ImpactStyle.Light });
        const result = await BarcodeScanner.startScan({
          cameraDirection: CameraDirection.BACK,
          targetedFormats: [SupportedFormat.QR_CODE],
        }); // start scanning and wait for a result
        await Haptics.impact({ style: ImpactStyle.Medium });

        this.ngZone.run(async () => {
          document.querySelector('body').classList.add('scanner-active');

          this.escaneando = false;
          document.querySelector('body').classList.remove('scanner-active');
          // if the result has content
          if (result.hasContent) {
            console.log(result.content); // log the raw scanned content
            this.codigo = result;
          }
        });
      }
    } catch (error) {
      if (error.message) {
        await Toast.show({
          text: error.message,
        });
      }
    }
  }

  async cancelar() {
    if (this.escaneando) {
      await BarcodeScanner.showBackground();
      await BarcodeScanner.stopScan();
      await Haptics.impact({ style: ImpactStyle.Heavy });
      this.ngZone.run(() => {
        this.escaneando = false;
      });
    }
  }
}
