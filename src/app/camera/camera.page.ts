import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { Camera, CameraResultType } from '@capacitor/camera';
import { Haptics, ImpactStyle } from '@capacitor/haptics';
import { Subject, takeUntil } from 'rxjs';
import { LoadingService } from '../services/loading.service';
import { ToastService } from '../services/toast.service';

@Component({
  selector: 'app-camera',
  templateUrl: './camera.page.html',
  styleUrls: ['./camera.page.scss'],
})
export class CameraPage implements OnInit, OnDestroy {
  imagen: string;
  cargando: boolean;
  unsubscribeAll: Subject<boolean>;
  constructor(
    private readonly ngZone: NgZone,
    private readonly loadingService: LoadingService,
    private readonly toast: ToastService
  ) {
    this.unsubscribeAll = new Subject<boolean>();
  }

  ngOnInit() {
    this.loadingService.loading$
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe({
        next: (cargando) => {
          this.cargando = cargando;
        },
      });
  }

  ngOnDestroy(): void {
    this.unsubscribeAll.next(true);
    this.unsubscribeAll.complete();
  }
  async camara() {
    await Haptics.impact({ style: ImpactStyle.Light });
    this.loadingService.start();
    try {
      const image = await Camera.getPhoto({
        quality: 90,
        allowEditing: false,
        promptLabelCancel: 'Cancelar',
        resultType: CameraResultType.Uri,
      });
      this.loadingService.stop();
      await Haptics.impact({ style: ImpactStyle.Medium });
      // image.webPath will contain a path that can be set as an image src.
      // You can access the original file using image.path, which can be
      // passed to the Filesystem API to read the raw data of the image,
      // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)

      // Can be set to the src of an image now
      this.ngZone.run(() => {
        this.imagen = image.webPath;
      });
      this.toast.success('Imágen obtenida.');
    } catch (error) {
      console.log('error camera', error);
      if (error?.message) {
        this.toast.error(error.message);
      }
      this.loadingService.stop();
    }
  }
}
