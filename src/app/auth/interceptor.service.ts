import { LoadingService } from './../services/loading.service';
/* eslint-disable @typescript-eslint/dot-notation */
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
@Injectable({
  providedIn: 'root',
})
export class InterceptorService implements HttpInterceptor {
  constructor(
    private auth: AuthService,
    private router: Router,
    private readonly loading: LoadingService
  ) {}

  intercept(
    req: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    let request = req;
    const headers = {};

    // inyectar el token jwt en los headers de las solicitudes...
    if (this.auth.token) {
      headers['authorization'] = `Bearer ${this.auth.token}`;
    }

    request = req.clone({
      setHeaders: headers,
    });
    this.loading.start();
    return next.handle(request).pipe(
      tap(() => {
        this.loading.stop();
      }),
      catchError((error: HttpErrorResponse) => {
        this.loading.stop();
        if (error.status === 401 && this.router.url !== '/login') {
          console.log('sacando a login');
          this.auth.logout();
        }
        return throwError(error);
      })
    );
  }
}
