export class UserModel {
  id: number;
  username: string;
  email: string;
  profile: string;
  validEmail: boolean;
  telefono: string;
  cp: string;
}
