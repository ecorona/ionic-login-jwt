import { UserModel } from './user.model';

export class LoginResponse {
  accessToken: string;
}
