import { Router } from '@angular/router';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginCredentials } from './model/login-credentials.model';
import { UserModel } from './model/user.model';
import { LoginResponse } from './model/login-response.model';
import { BehaviorSubject, Observable, of, tap } from 'rxjs';
import { ToastService } from '../services/toast.service';
import { Preferences } from '@capacitor/preferences';
import { PushService } from '../services/push.service';
import { SocketsService } from '../services/sockets.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  userSubject: BehaviorSubject<UserModel> = new BehaviorSubject<UserModel>(
    null
  );
  private privToken: string;
  constructor(
    private readonly http: HttpClient,
    private readonly router: Router,
    private readonly toast: ToastService,
    private readonly push: PushService,
    private readonly socketService: SocketsService
  ) {
    this.cargarToken().then((token: string) => {
      this.privToken = token;
      if (token) {
        this.getUser().subscribe();
        //al establecer el token, el socket se conecta solo.
        this.socketService.token = token;
      }
    });
  }

  get user$(): Observable<UserModel> {
    return this.userSubject.asObservable();
  }

  get token(): string {
    return this.privToken;
  }

  set token(token: string) {
    this.privToken = token;
    this.socketService.token = token;
    Preferences.set({
      key: 'token',
      value: token,
    }).then(() => {
      if (token) {
        return console.log('AuthService->Token almacenado');
      }
      console.log('AuthService->Token removido');
    });
  }

  private set user(user: UserModel) {
    this.userSubject.next(user);
  }

  async cargarToken(): Promise<string> {
    const { value } = await Preferences.get({ key: 'token' });
    return value;
  }
  login(credentials: LoginCredentials): Observable<LoginResponse> {
    return this.http
      .post<LoginResponse>(`${environment.apiUrl}/auth/login`, credentials)
      .pipe(
        tap((loginResponse) => {
          this.token = loginResponse.accessToken;
          this.getUser().subscribe();
        })
      );
  }

  isLoggedIn(): boolean {
    return !!this.token;
  }

  getUser(): Observable<UserModel> {
    if (!this.token) {
      return of(null);
    }
    return this.http.get<UserModel>(`${environment.apiUrl}/user`).pipe(
      tap((userSession) => {
        this.user = userSession;
        if (userSession?.id) {
          this.push.setExternalUserId(`${userSession.id}`);
        }
        if (userSession.profile) {
          this.push.setTags({
            profile: userSession.profile,
          });
        }
      })
    );
  }

  logout() {
    this.token = '';
    this.user = null;
    this.push.unSetExternalUserId();
    this.router.navigate(['/login'], {
      relativeTo: null,
      replaceUrl: true,
      skipLocationChange: true,
    });
    this.toast.success('Hasta luego!');
  }
}
