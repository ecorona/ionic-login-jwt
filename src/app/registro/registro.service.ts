import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RegistroResponse } from './registro-response.dto';
import { Observable } from 'rxjs';
import { RegistroRequestData } from './registro-request-data.dto';

@Injectable({
  providedIn: 'root',
})
export class RegistroService {
  constructor(private readonly http: HttpClient) {}

  registrar(data: RegistroRequestData): Observable<RegistroResponse> {
    return this.http.post<RegistroResponse>(
      environment.apiUrl + '/auth/subscribe',
      data
    );
  }
}
