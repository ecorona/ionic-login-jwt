export interface Error {
  path: string;
  timestamp: Date;
  name: string;
  message: string;
}

export interface ErrorResponse {
  status: number;
  statusText: string;
  url: string;
  ok: boolean;
  name: string;
  message: string;
  error: Error;
}
