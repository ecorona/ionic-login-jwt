export class RegistroResponse {
  username?: string;
  email?: string;
  deletedAt?: null;
  telefono?: null;
  cp?: null;
  id?: number;
  createdAt?: Date;
  updatedAt?: Date;
  version?: number;
  active?: boolean;
  validEmail?: boolean;
}
