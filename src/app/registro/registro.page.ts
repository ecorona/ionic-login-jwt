import { RegistroRequestData } from './registro-request-data.dto';
import { RegistroService } from './registro.service';
import { Component, OnInit } from '@angular/core';
import { Preferences } from '@capacitor/preferences';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { emailValidator } from '../validators/email.validators';
import { matchValidator } from '../validators/match.validators';
import { RegistroResponse } from './registro-response.dto';
import { ErrorResponse } from './registro-error-response.dto';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  formRegistro: FormGroup;
  isSubmitted = false;
  error: string;
  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly registroService: RegistroService,
    private readonly router: Router
  ) {}
  get errorControl() {
    return this.formRegistro.controls;
  }

  get passwordMatchError() {
    return (
      this.formRegistro.getError('mismatch') &&
      this.formRegistro.get('confirmPassword')?.touched
    );
  }

  ngOnInit() {
    this.formRegistro = this.formBuilder.group(
      {
        username: ['', [Validators.required, Validators.minLength(5)]],
        email: ['', [Validators.required, emailValidator]],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', [Validators.required, Validators.minLength(6)]],
      },
      {
        validators: [matchValidator('password', 'confirmPassword')],
      }
    );
  }

  async submitForm() {
    this.isSubmitted = true;
    if (!this.formRegistro.valid) {
      console.log('El formulario de registro no es correcto.');
      return false;
    } else {
      const formData: RegistroRequestData = this.formRegistro.getRawValue();
      this.error = '';
      console.log('form:', formData);
      this.registroService.registrar(formData).subscribe({
        next: async (response: RegistroResponse) => {
          console.log('registroService.registrar->response: ', response);
          //pasarlo a la página de verificacion y borrar la anterior.
          await Preferences.set({
            key: 'emailRegistro',
            value: formData.email,
          });
          this.router.navigate(['/verificacion'], {
            replaceUrl: true,
            skipLocationChange: true,
          });
        },
        error: (errorResponse: ErrorResponse) => {
          this.error =
            errorResponse.error?.message ||
            errorResponse.message ||
            'No se pudo efectuar el registro, inténtelo más tarde.';
          console.log(
            'registroService.registrar->error-response: ',
            errorResponse
          );
        },
      });
    }
  }
}
